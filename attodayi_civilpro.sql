/*
SQLyog Professional v12.5.1 (64 bit)
MySQL - 10.4.6-MariaDB : Database - attodayi_civilpro
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`attodayi_civilpro` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `attodayi_civilpro`;

/*Table structure for table `client` */

DROP TABLE IF EXISTS `client`;

CREATE TABLE `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(255) NOT NULL,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `user_name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `client_id` varchar(128) NOT NULL,
  `phone_no` varchar(255) NOT NULL,
  `company` varchar(128) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `address` varchar(300) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `state` varchar(300) NOT NULL,
  `country` varchar(300) NOT NULL,
  `pincode` varchar(300) NOT NULL,
  `time_set` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `client` */

insert  into `client`(`id`,`img`,`first_name`,`last_name`,`user_name`,`email`,`password`,`client_id`,`phone_no`,`company`,`birthday`,`address`,`gender`,`state`,`country`,`pincode`,`time_set`) values 
(2,'https://cdn4.vectorstock.com/i/1000x1000/12/13/construction-worker-icon-person-profile-avatar-vector-15541213.jpg ','James ','Reimer','travis@silverstoneexcavating.com','james@reimerbuilt.com','123456','14','6045566619','Reimer Group','','','','','','','1557258657'),
(3,'https://cdn4.vectorstock.com/i/1000x1000/12/13/construction-worker-icon-person-profile-avatar-vector-15541213.jpg ','Ravinder','Dhaliwal','travis@silverstoneexcavating.com','Ravi.uniquedrywall@gmail.com','123456','14','6043097372','Dhaliwal Developers','','','','','','','1557259476'),
(4,'https://cdn4.vectorstock.com/i/1000x1000/12/13/construction-worker-icon-person-profile-avatar-vector-15541213.jpg ','Bryan','Smyth','travis@silverstoneexcavating.com','bryan@rkdi.com','123456','','6043188074','Redekop Kroeker Development Inc.','','','','','','','1557259674'),
(5,'https://cdn4.vectorstock.com/i/1000x1000/12/13/construction-worker-icon-person-profile-avatar-vector-15541213.jpg ','Gerald ','Heinrichs','travis@silverstoneexcavating.com','gerald@heinrichsdevelopments.com','123456','14','6043097537','Heinrichs Developments','','','','','','','1557260002'),
(6,'https://cdn4.vectorstock.com/i/1000x1000/12/13/construction-worker-icon-person-profile-avatar-vector-15541213.jpg ','TJ','Sidhu','travis@silverstoneexcavating.com','tj@goldedgeproperties.com','123456','14','6043025223','Goldedge Properties','','','','','','','1557260233'),
(7,'https://cdn4.vectorstock.com/i/1000x1000/12/13/construction-worker-icon-person-profile-avatar-vector-15541213.jpg ','Raja','Bains','travis@silverstoneexcavating.com','pcl@hotmail.ca','123456','14','6045629680','Pragati Construction','','','','','','','1557260347'),
(8,'https://cdn4.vectorstock.com/i/1000x1000/12/13/construction-worker-icon-person-profile-avatar-vector-15541213.jpg ','Randy','Redekop','travis@silverstoneexcavating.com','randy@reddale.ca','123456','14','7788088077','Reddale Construction','','','','','','','1557260432'),
(9,'https://cdn4.vectorstock.com/i/1000x1000/12/13/construction-worker-icon-person-profile-avatar-vector-15541213.jpg ','James','Redekop','travis@silverstoneexcavating.com','James@redekopdevelopment.com','123456','14','6048350217','Redekop Development Corp.','','','','','','','1557260540'),
(10,'https://cdn4.vectorstock.com/i/1000x1000/12/13/construction-worker-icon-person-profile-avatar-vector-15541213.jpg ','Radu','Manor','travis@silverstoneexcavating.com','rmanor@houle.ca','123456','14','7782314257','Houle Security','','','','','','','1557260635'),
(11,'https://cdn4.vectorstock.com/i/1000x1000/12/13/construction-worker-icon-person-profile-avatar-vector-15541213.jpg ','Darren','York','travis@silverstoneexcavating.com','darren@yorkbuilt.net','123456','14','6046909675','York Built','','','','','','','1557260813'),
(12,'https://cdn4.vectorstock.com/i/1000x1000/12/13/construction-worker-icon-person-profile-avatar-vector-15541213.jpg ','Darren ','York','travis@silverstoneexcavating.com','darren@yorkbuilt.net','123456','14','6046909675','Westlund Group','','','','','','','1557261056');

/*Table structure for table `client123` */

DROP TABLE IF EXISTS `client123`;

CREATE TABLE `client123` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `client123` */

insert  into `client123`(`id`,`first_name`,`last_name`,`email`) values 
(1,'James','Warmerdam','james@silverstoneexcavating.com'),
(2,'Tom','Percival','tom@silverstoneexcavating.com'),
(3,'Tanner','Redekop','tanner@silverstoneexcavating.com'),
(4,'Thomas','Redekop','thomas@silverstoneexcavating.com'),
(5,'Travis','Redekop','travis@silverstoneexcavating.com'),
(6,'Kevin','Weed','lilkevy222@hotmail.com'),
(7,'Nick','Kunze','kunze.nicholas@gmail.com'),
(8,'Tim','Hunt','mrthunt2u@yahoo.ca'),
(9,'Jonny','Kotanen','jkotanen@gmail.com'),
(10,'Brandon','Litum','brandon@silverstoneexcavating.com'),
(11,'Ewan','Fafard','ewan19@hotmail.com'),
(12,'Ivan','Ferris','val_ivan@telus.net'),
(13,'Robbie','New','rob@silverstoneexcavating.com');

/*Table structure for table `company_details` */

DROP TABLE IF EXISTS `company_details`;

CREATE TABLE `company_details` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(300) NOT NULL,
  `contact_person` varchar(300) NOT NULL,
  `address` varchar(300) NOT NULL,
  `country` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state/province` varchar(250) NOT NULL,
  `postalcode` varchar(300) NOT NULL,
  `email` varchar(300) NOT NULL,
  `phone_number` varchar(300) NOT NULL,
  `mobile_number` varchar(300) NOT NULL,
  `fax` varchar(300) NOT NULL,
  `website_url` varchar(300) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `company_details` */

/*Table structure for table `department` */

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(256) NOT NULL,
  `time_set` varchar(255) NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `department` */

insert  into `department`(`department_id`,`department_name`,`time_set`) values 
(1,'IT','1556981210'),
(3,'Excavation','1557254073'),
(4,'Foreman','1575047316');

/*Table structure for table `designation` */

DROP TABLE IF EXISTS `designation`;

CREATE TABLE `designation` (
  `designation_id` int(11) NOT NULL AUTO_INCREMENT,
  `designation_name` varchar(256) NOT NULL,
  `department_name` varchar(255) NOT NULL,
  `time_set` varchar(255) NOT NULL,
  PRIMARY KEY (`designation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `designation` */

insert  into `designation`(`designation_id`,`designation_name`,`department_name`,`time_set`) values 
(1,'Manager','3','1556981220'),
(4,'Foreman','3','1557254140'),
(5,'Operator','3','1557254153'),
(6,'Grademan','3','1557254177'),
(7,'Labourer','3','1557254189'),
(8,'Mechanic','3','1557258169');

/*Table structure for table `dispatch_log` */

DROP TABLE IF EXISTS `dispatch_log`;

CREATE TABLE `dispatch_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) DEFAULT NULL,
  `start_time` varchar(100) DEFAULT NULL,
  `job_site` varchar(100) DEFAULT NULL,
  `equipment_id` int(11) DEFAULT NULL,
  `scope_work` varchar(100) DEFAULT NULL,
  `special_req` varchar(100) DEFAULT NULL,
  `trucks` varchar(100) DEFAULT NULL,
  `material_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `dispatch_date` date DEFAULT NULL,
  `Project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=394 DEFAULT CHARSET=utf8;

/*Data for the table `dispatch_log` */

insert  into `dispatch_log`(`id`,`emp_id`,`start_time`,`job_site`,`equipment_id`,`scope_work`,`special_req`,`trucks`,`material_id`,`quantity`,`status`,`dispatch_date`,`Project_id`) values 
(1,16,'20:00','',128,'Testing ','Testing','Testign',38,10,'Dispatched','2019-05-07',1),
(5,1,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(6,2,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(7,3,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(8,4,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(9,5,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(10,6,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(11,7,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(12,8,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(13,9,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(14,10,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(15,11,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(16,12,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(17,13,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(18,17,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(19,18,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(20,19,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(21,20,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(22,21,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(23,22,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(24,23,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(25,24,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(26,25,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(27,26,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(28,27,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(29,28,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(30,29,'','',NULL,'','','',NULL,0,'Dispatched','2019-05-09',NULL),
(36,1,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(37,2,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(38,3,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(39,4,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(40,5,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(41,6,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(42,7,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(43,8,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(44,9,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(45,10,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(46,11,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(47,12,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(48,13,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(49,33,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(50,34,'','',NULL,'','','',NULL,0,'New','2019-05-10',NULL),
(51,1,'','',NULL,'','','',NULL,0,'New','2019-05-12',NULL),
(52,2,'','',NULL,'','','',NULL,0,'New','2019-05-12',NULL),
(53,3,'','',NULL,'','','',NULL,0,'New','2019-05-12',NULL),
(54,4,'','',NULL,'','','',NULL,0,'New','2019-05-12',NULL),
(55,5,'','',NULL,'','','',NULL,0,'New','2019-05-12',NULL),
(56,6,'','',NULL,'','','',NULL,0,'New','2019-05-12',NULL),
(57,7,'','',NULL,'','','',NULL,0,'New','2019-05-12',NULL),
(58,8,'','',NULL,'','','',NULL,0,'New','2019-05-12',NULL),
(59,9,'','',NULL,'','','',NULL,0,'New','2019-05-12',NULL),
(60,10,'','',NULL,'','','',NULL,0,'New','2019-05-12',NULL),
(61,11,'','',NULL,'','','',NULL,0,'New','2019-05-12',NULL),
(62,12,'','',NULL,'','','',NULL,0,'New','2019-05-12',NULL),
(63,13,'','',NULL,'','','',NULL,0,'New','2019-05-12',NULL),
(64,35,'','',NULL,'','','',NULL,0,'New','2019-05-12',NULL),
(66,1,'','',NULL,'','','',NULL,0,'New','2019-05-13',NULL),
(67,2,'','',NULL,'','','',NULL,0,'New','2019-05-13',NULL),
(68,3,'','',NULL,'','','',NULL,0,'New','2019-05-13',NULL),
(69,4,'','',NULL,'','','',NULL,0,'New','2019-05-13',NULL),
(70,5,'','',NULL,'','','',NULL,0,'New','2019-05-13',NULL),
(71,6,'','',NULL,'','','',NULL,0,'New','2019-05-13',NULL),
(72,7,'','',NULL,'','','',NULL,0,'New','2019-05-13',NULL),
(73,8,'','',NULL,'','','',NULL,0,'New','2019-05-13',NULL),
(74,9,'','',NULL,'','','',NULL,0,'New','2019-05-13',NULL),
(75,10,'','',NULL,'','','',NULL,0,'New','2019-05-13',NULL),
(76,11,'','',NULL,'','','',NULL,0,'New','2019-05-13',NULL),
(77,12,'','',NULL,'','','',NULL,0,'New','2019-05-13',NULL),
(78,13,'','',NULL,'','','',NULL,0,'New','2019-05-13',NULL),
(79,35,'','',NULL,'','','',NULL,0,'New','2019-05-13',NULL),
(81,1,'','',NULL,'','','',NULL,0,'New','2019-05-14',NULL),
(82,2,'','',NULL,'','','',NULL,0,'New','2019-05-14',NULL),
(83,3,'','',NULL,'','','',NULL,0,'New','2019-05-14',NULL),
(84,4,'','',NULL,'','','',NULL,0,'New','2019-05-14',NULL),
(85,5,'','',NULL,'','','',NULL,0,'New','2019-05-14',NULL),
(86,6,'','',NULL,'','','',NULL,0,'New','2019-05-14',NULL),
(87,7,'','',NULL,'','','',NULL,0,'New','2019-05-14',NULL),
(88,8,'','',NULL,'','','',NULL,0,'New','2019-05-14',NULL),
(89,9,'','',NULL,'','','',NULL,0,'New','2019-05-14',NULL),
(90,10,'','',NULL,'','','',NULL,0,'New','2019-05-14',NULL),
(91,11,'','',NULL,'','','',NULL,0,'New','2019-05-14',NULL),
(92,12,'','',NULL,'','','',NULL,0,'New','2019-05-14',NULL),
(93,13,'','',NULL,'','','',NULL,0,'New','2019-05-14',NULL),
(94,35,'','',NULL,'','','',NULL,0,'New','2019-05-14',NULL),
(96,5,'00:12','',149,'Good ','12','15',53,123,'Dispatched','2019-05-21',2),
(97,5,'','',-1,'Complete','','',-1,0,'Dispatched','2019-11-21',2),
(98,2,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(99,3,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(100,4,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(101,5,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(102,6,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(103,7,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(104,8,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(105,9,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(106,10,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(107,11,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(108,12,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(109,13,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(110,14,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(111,15,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(112,16,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(113,17,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-21',NULL),
(128,1,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(129,2,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(130,3,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(131,4,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(132,5,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(133,6,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(134,7,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(135,8,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(136,9,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(137,10,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(138,11,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(139,12,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(140,13,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(141,14,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(142,15,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(143,16,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(144,17,'','',NULL,'','','',NULL,0,'New','2019-11-13',NULL),
(159,1,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-22',NULL),
(160,2,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(161,3,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(162,4,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(163,5,'19:00','',-1,'a','','',-1,0,'Dispatched','2019-11-22',3),
(164,6,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(165,7,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(166,8,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(167,9,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(168,10,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(169,11,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(170,12,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(171,13,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(172,14,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(173,15,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(174,16,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(175,17,'','',NULL,'','','',NULL,0,'New','2019-11-22',NULL),
(190,1,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(191,2,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(192,3,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(193,4,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(194,5,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(195,6,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(196,7,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(197,8,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(198,9,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(199,10,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(200,11,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(201,12,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(202,13,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(203,14,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(204,15,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(205,16,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(206,17,'','',NULL,'','','',NULL,0,'New','2019-11-23',NULL),
(221,1,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(222,2,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(223,3,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(224,4,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(225,5,'','',-1,'a','','',-1,0,'Dispatched','2019-11-25',2),
(226,6,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(227,7,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(228,8,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(229,9,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(230,10,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(231,11,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(232,12,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(233,13,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(234,14,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(235,15,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(236,16,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(237,17,'','',NULL,'','','',NULL,0,'New','2019-11-25',NULL),
(252,1,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(253,2,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(254,3,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(255,4,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(256,5,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(257,6,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(258,7,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(259,8,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(260,9,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(261,10,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(262,11,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(263,12,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(264,13,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(265,14,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(266,15,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(267,16,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(268,17,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(269,18,'','',NULL,'','','',NULL,0,'Dispatched','2019-11-29',NULL),
(283,1,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(284,2,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(285,3,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(286,4,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(287,5,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(288,6,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(289,7,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(290,8,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(291,9,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(292,10,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(293,11,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(294,12,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(295,13,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(296,14,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(297,15,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(298,16,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(299,17,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(300,18,'','',NULL,'','','',NULL,0,'New','2019-11-30',NULL),
(314,1,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(315,2,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(316,3,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(317,4,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(318,5,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(319,6,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(320,7,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(321,8,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(322,9,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(323,10,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(324,11,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(325,12,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(326,13,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(327,14,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(328,15,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(329,16,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(330,17,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(331,18,'','',NULL,'','','',NULL,0,'New','2019-12-01',NULL),
(332,1,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(333,2,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(334,3,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(335,4,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(336,5,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(337,6,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(338,7,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(339,8,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(340,9,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(341,10,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(342,11,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(343,12,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(344,13,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(345,14,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(346,15,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(347,16,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(348,17,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(349,18,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(350,19,'','',NULL,'','','',NULL,0,'Dispatched','2019-12-17',NULL),
(363,1,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(364,2,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(365,3,'07:00','',-1,'1','','',-1,0,'Dispatched','2019-12-18',3),
(366,4,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(367,5,'07:00','',149,'Work','','1',53,2,'Dispatched','2019-12-18',3),
(368,6,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(369,7,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(370,8,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(371,9,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(372,10,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(373,11,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(374,12,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(375,13,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(376,14,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(377,15,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(378,16,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(379,17,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(380,18,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL),
(381,19,'','',NULL,'','','',NULL,0,'New','2019-12-18',NULL);

/*Table structure for table `employee` */

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `empl_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `confirm_pass` varchar(255) NOT NULL,
  `employee_id` varchar(255) NOT NULL,
  `joining_date` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `time_set` varchar(255) NOT NULL,
  `hourly_rate` int(11) DEFAULT NULL,
  `noti_status` int(11) DEFAULT 1,
  PRIMARY KEY (`empl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `employee` */

insert  into `employee`(`empl_id`,`first_name`,`last_name`,`username`,`email`,`password`,`confirm_pass`,`employee_id`,`joining_date`,`phone`,`img`,`company`,`designation`,`device_id`,`device_type`,`time_set`,`hourly_rate`,`noti_status`) values 
(1,'James','Warmerdam','jameswarmerdam','james@silverstoneexcavating.com','e10adc3949ba59abbe56e057f20f883e','e10adc3949ba59abbe56e057f20f883e','1','28/04/2019','9953813100','','Global Technologies','1','123456','iOS','',10,1),
(2,'Tom','Percival','tompercival','tom@silverstoneexcavating.com','e10adc3949ba59abbe56e057f20f883e','e10adc3949ba59abbe56e057f20f883e','1','28/04/2019','9953813100','','Global Technologies','1','','','',10,1),
(3,'Tanner','Redekop','tannerredekop','tanner@silverstoneexcavating.com','fcea920f7412b5da7be0cf42b8c93759','d41d8cd98f00b204e9800998ecf8427e','1','28/04/2019','9953813100','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190521204556.jpg','Global Technologies','1','dWGv0K0AWIU:APA91bE2oqg1fCI0IBy1MjHu9jmKIYTsT6u9D9SgqoK9_B3edcq2IwqECqXPxxVs97c3Rf8Ez79UY2j_0ZxTfR2MRa0xswTSnkUo4PHpeZUrDDN4Bn6coON7XCUeVfjgxvoAhJIqkwWr','Android','',2,1),
(4,'Thomas','Redekop','thomasredekop','thomas@silverstoneexcavating.com','e10adc3949ba59abbe56e057f20f883e','e10adc3949ba59abbe56e057f20f883e','1','28/04/2019','9953813100','http://104.248.30.138/civilpro/rest_api/v1/uploads/uploads/avatar.png','Global Technologies','1','e5TX-figZas:APA91bGIQqOK4nhxJKeF1SZm5UcuQ4DS4umQslO7KqvwNCZ33eyHnBO5PTIsmXaA5xSyaSyZBACMxveDtFreYPy5FnhLE9UewdYqlBt4EXJnlSYsvHoMx1mUUCuuYD4KMJ86LHLwYpMm','Android','',2,1),
(5,'Travis','Redekop','travisredekop','travis@silverstoneexcavating.com','d41d8cd98f00b204e9800998ecf8427e','d41d8cd98f00b204e9800998ecf8427e','1','28/04/2019','9953813100','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1574349502.jpg','Global Technologies','1','eAloX2Xhk9c:APA91bEk5E-347NssvST_f06L-DUKKxmPAspkEERfzdilocCGETkVJqzCzFRBUD4967v5dt_kV65u1Xbh8TL6FdKvYcuFS43aX3cpiEa8dMoIMOLEsBiKR-WdVv7LVKo-9zC4JBwhcto','Android','',1,1),
(6,'Kevin','Weed','kevinweed','lilkevy222@hotmail.com','e10adc3949ba59abbe56e057f20f883e','e10adc3949ba59abbe56e057f20f883e','1','28/04/2019','9953813100','','Global Technologies','1','','','',10,1),
(7,'Nick','Kunze','nickkunze','kunze.nicholas@gmail.com','e10adc3949ba59abbe56e057f20f883e','e10adc3949ba59abbe56e057f20f883e','1','28/04/2019','9953813100','','Global Technologies','1','','','',10,1),
(8,'Tim','Hunt','timhunt','mrthunt2u@yahoo.ca','e10adc3949ba59abbe56e057f20f883e','e10adc3949ba59abbe56e057f20f883e','1','28/04/2019','9953813100','','Global Technologies','1','','','',10,1),
(9,'Jonny','Kotanen','jonnykotanen','jkotanen@gmail.com','e10adc3949ba59abbe56e057f20f883e','e10adc3949ba59abbe56e057f20f883e','1','28/04/2019','9953813100','','Global Technologies','1','','','',10,1),
(10,'Brandon','Litum','brandonlitum','brandon@silverstoneexcavating.com','e10adc3949ba59abbe56e057f20f883e','e10adc3949ba59abbe56e057f20f883e','1','28/04/2019','9953813100','','Global Technologies','1','','','',10,1),
(11,'Ewan','Fafard','ewanfafard','ewan19@hotmail.com','e10adc3949ba59abbe56e057f20f883e','e10adc3949ba59abbe56e057f20f883e','1','28/04/2019','9953813100','','Global Technologies','1','','','',10,1),
(12,'Ivan','Ferris','ivanferris','val_ivan@telus.net','e10adc3949ba59abbe56e057f20f883e','e10adc3949ba59abbe56e057f20f883e','1','28/04/2019','9953813100','','Global Technologies','1','','','',10,1),
(13,'Robbie','New','robbienew','rob@silverstoneexcavating.com','d41d8cd98f00b204e9800998ecf8427e','d41d8cd98f00b204e9800998ecf8427e','1','28/04/2019','9953813100','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1558363211.jpg','Global Technologies','1','','','',1,1),
(14,'Jeff','Begley ','','1jeffbegley@gmail','1Husaberg450','','','','','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1561570733.jpg','','','','','',NULL,1),
(15,'Nico','Badenhorst ','','ndbadenhorst@gmail.com','Nico2008','','','','','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1562244416.jpg','','','','','',NULL,0),
(16,'a','go','','mydogmodou@yahoo.com','123456','','','','','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1571276654.jpg','','','','','',NULL,1),
(17,'Morgan ','Mann ','','morganm@pensar.com.au','Pensar73','','','','','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1573076954.jpg','','','','','',NULL,1),
(18,'don','smith','','smitty7586@yahoo.com','Pason-55','','','','','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1574469231.jpg','','','','','',NULL,1),
(19,'Thomas','Redekop','test','smart_mine321@hotmail.com','e10adc3949ba59abbe56e057f20f883e','e10adc3949ba59abbe56e057f20f883e','19','04/12/2019','321456789','https://cdn4.vectorstock.com/i/1000x1000/12/13/construction-worker-icon-person-profile-avatar-vector-15541213.jpg','Delta Infotech','1','','','1575477955',15,1);

/*Table structure for table `excavator` */

DROP TABLE IF EXISTS `excavator`;

CREATE TABLE `excavator` (
  `excavator_id` int(11) NOT NULL AUTO_INCREMENT,
  `operator` varchar(255) NOT NULL,
  `site_area` varchar(250) NOT NULL,
  `hour_start` varchar(250) NOT NULL,
  `hour_finish` varchar(250) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `fuel_lavel` varchar(250) NOT NULL,
  `engine_oil_lavel` varchar(250) NOT NULL,
  `oil_lavel` varchar(250) NOT NULL,
  `radiator_lavel` varchar(250) NOT NULL,
  `transmission_lavel` varchar(250) NOT NULL,
  `fluids_leak` varchar(250) NOT NULL,
  `fluid_level_comment` varchar(250) NOT NULL,
  `air_conditioning` varchar(250) NOT NULL,
  `bucket_teeth_pins` varchar(250) NOT NULL,
  `cleaning_products` varchar(250) NOT NULL,
  `damage_report` varchar(250) NOT NULL,
  `fire_extinguisher` varchar(250) NOT NULL,
  `first_aid_kit` varchar(250) NOT NULL,
  `general_defects` varchar(250) NOT NULL,
  `grease_lines_pins` varchar(250) NOT NULL,
  `hand_rails_door_handles` varchar(250) NOT NULL,
  `horn` varchar(250) NOT NULL,
  `hydraulic_hoses` varchar(250) NOT NULL,
  `lights` varchar(250) NOT NULL,
  `mirrors` varchar(250) NOT NULL,
  `panel_damage` varchar(250) NOT NULL,
  `radiator` varchar(250) NOT NULL,
  `radiator_hoses` varchar(250) NOT NULL,
  `seat_seatbelts` varchar(250) NOT NULL,
  `slew_motor_oil` varchar(250) NOT NULL,
  `tracks_chains_shoes` varchar(250) NOT NULL,
  `windows_wipers` varchar(250) NOT NULL,
  `additional_notes` varchar(250) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`excavator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

/*Data for the table `excavator` */

insert  into `excavator`(`excavator_id`,`operator`,`site_area`,`hour_start`,`hour_finish`,`date`,`fuel_lavel`,`engine_oil_lavel`,`oil_lavel`,`radiator_lavel`,`transmission_lavel`,`fluids_leak`,`fluid_level_comment`,`air_conditioning`,`bucket_teeth_pins`,`cleaning_products`,`damage_report`,`fire_extinguisher`,`first_aid_kit`,`general_defects`,`grease_lines_pins`,`hand_rails_door_handles`,`horn`,`hydraulic_hoses`,`lights`,`mirrors`,`panel_damage`,`radiator`,`radiator_hoses`,`seat_seatbelts`,`slew_motor_oil`,`tracks_chains_shoes`,`windows_wipers`,`additional_notes`,`image`) values 
(1,'testing','Testing','10','1O','2019-03-19 00:00:00','true','true','true','true','true','true','abc','true','true','true','true','true','true','true','true','','true','true','true','true','true','true','true','true','true','true','true','NOTE','574684556.730614.png'),
(2,'TESITNG','TESTING','10','10','2019-03-19 00:00:00','true','true','true','true','true','true','abc','true','true','true','true','true','true','true','true','','true','true','true','true','true','true','true','true','true','true','true','note','574684799.607449.png'),
(3,'Tesat','tester','12','12','2019-03-19 00:00:00','true','true','true','true','true','true','abc','true','true','true','true','true','true','true','true','','true','true','true','true','true','true','true','true','true','true','true','Note','574688797.661095.png'),
(4,'fgfggfdg','fgfdg','12','12','2019-03-22 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','1','1','1','1','1','1','1','1','aww we','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1553251077.jpg'),
(5,'fgfggfdg','fgfdg','125','125','2019-03-22 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','1','1','1','1','1','1','1','1','aww we','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1553251158.jpg'),
(6,'hhdhd','dyyy','5','5','2019-04-04 00:00:00','0','1','0','1','1','1','abc','1','1','1','1','0','1','1','0','','1','0','1','1','1','1','0','1','1','1','1','jfnfnf','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1554335616.jpg'),
(7,'f','fff','2','23','2019-04-17 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','1','1','1','1','1','1','1','1','jdhd','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1555465893.jpg'),
(8,'gggh','fffg','12','15','2019-04-24 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','0','','0','1','1','1','1','1','1','1','1','1','1','gggh','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1556110283.jpg'),
(9,'Test','Demo','20','29','2019-05-10 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','1','1','1','1','1','1','1','1','test','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1557475379.jpg'),
(10,'test1','test2','13','19','2019-05-15 00:00:00','1','0','1','0','1','0','1','0','1','1','1','0','0','0','0','','1','1','1','1','1','0','0','0','1','1','1','1','\"\"'),
(11,'test1','test2','13','19','0000-00-00 00:00:00','1','0','1','0','1','0','1','0','1','1','1','0','0','0','0','','1','1','1','1','1','0','0','0','1','1','1','1','\"\"'),
(12,'test1','test2','13','19','0000-00-00 00:00:00','1','0','1','0','1','0','1','0','1','1','1','0','0','0','0','','1','1','1','1','1','0','0','0','1','1','1','1','\"\"'),
(13,'test1','test2','13','19','0000-00-00 00:00:00','1','0','1','0','1','0','1','0','1','1','1','0','0','0','0','','1','1','1','1','1','0','0','0','1','1','1','1','\"\"'),
(14,'test operator','test site area','9','15','2019-05-15 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','1','1','1','1','1','1','1','1','test note','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1557922187.jpg'),
(15,'test1','test2','13','19','2019-05-15 00:00:00','1','0','1','0','1','0','1','0','1','1','1','0','0','0','0','','1','1','1','1','1','0','0','0','1','1','1','1','\"\"'),
(16,'test1','test2','13','19','2019-05-15 00:00:00','1','0','1','0','1','0','1','0','1','1','1','0','0','0','0','','1','1','1','1','1','0','0','0','1','1','1','1','\"tedt\"'),
(17,'test and','test sit and','9','15','0000-00-00 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','1','1','1','1','1','1','1','1','test note','test'),
(18,'test 1','tests1','5','9','0000-00-00 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','1','1','1','1','1','1','1','1','test note1','test'),
(19,'test 2','test s2','2','9','0000-00-00 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','1','1','1','1','1','1','1','1','test n2','test'),
(20,'test o4','test s4','3','9','0000-00-00 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','1','1','1','1','1','1','1','1','tes note4','test'),
(21,'test o4','test s4','3','9','0000-00-00 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','1','1','1','1','1','1','1','1','tes note4','test'),
(22,'r','g','5','9','2019-05-15 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','1','1','1','1','1','1','1','1','fb','test'),
(23,'test','sutearea','8','10','2019-05-15 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','1','1','1','1','1','1','1','1','test note','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1557926081.jpg'),
(24,'test','sutearea','8','10','2019-05-15 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','1','1','1','1','1','1','1','1','test note','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1557926098.jpg'),
(25,'eo19','demo','5','9','2019-05-15 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','1','1','1','1','1','1','1','1','teat note','test'),
(26,'d','v','9','15','2019-05-16 00:00:00','False','False','False','False','False','False','abc','False','False','False','False','False','False','False','False','','False','False','False','False','False','False','False','False','False','False','False','text','test'),
(27,'testing','testing dite','10','16','2019-05-16 00:00:00','True','False','True','False','True','False','abc','False','True','False','True','False','True','False','True','','True','False','True','False','True','False','True','False','True','False','True','testing notes','test'),
(28,'test','test','3','9','2019-05-16 00:00:00','False','True','True','False','True','True','abc','True','True','False','True','True','False','True','False','','False','True','True','True','True','True','True','True','True','False','True','test','test'),
(29,'g','y','5','9','2019-05-18 00:00:00','True','True','True','True','True','True','abc','True','True','True','True','True','True','True','True','','True','True','True','True','True','True','True','True','True','True','True','jgh','test'),
(30,'test','test12','1','9','2019-05-18 00:00:00','True','False','True','False','True','True','abc','True','True','True','True','True','True','True','True','','True','False','True','False','True','False','True','False','False','True','False','note','android.graphics.Bitmap@184e8d2'),
(31,'hdhd','dhdhdhd','8','98','2019-05-19 00:00:00','True','True','False','True','True','True','abc','True','True','True','True','True','True','True','True','','True','True','True','True','True','True','True','True','True','True','True','hrhr','android.graphics.Bitmap@c44667e'),
(32,'sy','yg f','55','5','2019-05-19 00:00:00','True','True','True','True','True','True','abc','True','True','True','True','True','True','True','True','','True','True','True','True','True','True','True','True','True','True','True','frtt','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190519153638.jpg'),
(33,'dhud','uru','86','88','2019-05-19 00:00:00','True','True','True','True','True','True','abc','True','True','True','True','True','True','True','True','','True','True','True','True','True','True','True','True','True','True','True','dh hf','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190519153906.jpg'),
(34,'ydyd','duus','8','8','2019-05-19 00:00:00','True','True','True','True','True','True','abc','True','True','True','True','True','True','True','True','','True','True','True','True','True','True','True','True','True','True','True','dj jd','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190519154410.jpg'),
(35,'tet','welc','5','9','2019-05-20 00:00:00','True','True','True','True','True','True','abc','True','True','True','True','True','True','True','True','','True','True','False','True','False','True','True','False','True','False','True','welcom','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190520120626.jpg'),
(36,'rv','fn.fbg','2','9','2019-05-20 00:00:00','True','False','False','True','True','True','abc','True','True','True','True','True','True','True','False','','True','False','True','True','False','True','True','False','True','True','True','bote','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190520124003.jpg'),
(37,'tehd','fgkkg','6','9','2019-05-20 00:00:00','True','True','True','True','True','True','abc','True','True','True','True','True','True','True','False','','True','False','True','True','True','False','True','True','True','True','True','wi','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190520124025.jpg'),
(38,'test','vn','9','10','2019-05-20 00:00:00','True','True','True','True','True','True','abc','True','True','True','True','True','True','False','True','','False','False','True','False','True','True','False','True','False','False','True','bottom help','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190520124727.jpg'),
(39,'test','mumbai','8','15','2019-05-20 00:00:00','True','True','False','True','True','False','abc','True','False','True','True','True','False','True','True','','True','True','False','True','True','False','True','False','True','True','True','note back','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190520170329.jpg'),
(40,'test1','test2','13','19','2019-05-15 00:00:00','1','0','1','0','1','0','1','0','1','1','1','0','0','0','0','','1','1','1','1','1','0','0','0','1','1','1','1','\"\"'),
(41,'sgs','dege','5','5','2019-05-21 00:00:00','True','True','True','True','True','True','abc','True','True','True','True','True','True','True','True','','True','True','True','True','True','True','True','True','True','True','True','gde','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190521211513.jpg'),
(42,'testing','test site area','4','9','2019-05-23 00:00:00','True','False','False','True','True','False','abc','True','False','True','True','True','False','True','True','','True','True','False','True','True','True','False','True','True','True','False','testing note','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190523112215.jpg'),
(43,'test','test site','4','9','2019-05-23 00:00:00','True','False','True','False','True','False','abc','True','False','True','True','False','True','False','True','','True','False','True','True','False','True','True','False','True','False','True','testing note','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190523114648.jpg'),
(44,'testing','test','10','19','2019-07-05 00:00:00','False','True','False','True','False','True','abc','False','True','False','False','True','False','True','False','','True','False','False','True','False','True','False','True','False','False','True','tested note','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190705172549.jpg'),
(45,'Tanner Redekop','Duffy 47-17','3200','3208','2020-01-08 00:00:00','1','1','1','1','1','1','abc','1','1','1','1','1','1','1','1','','1','1','1','0','1','1','1','1','1','1','1','tie wire holding bucket pin in.','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1578503236.jpg');

/*Table structure for table `field_report` */

DROP TABLE IF EXISTS `field_report`;

CREATE TABLE `field_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employe_id` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `project` varchar(100) NOT NULL,
  `delay` varchar(100) NOT NULL,
  `scope_work` varchar(100) NOT NULL,
  `picture` text NOT NULL,
  `time_set` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `field_report` */

insert  into `field_report`(`id`,`employe_id`,`date`,`project`,`delay`,`scope_work`,`picture`,`time_set`) values 
(16,'5','2019-05-20','2','3','Thank u iOS','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1558353382.jpg','1558353453'),
(18,'5','2019-05-20','3','2','Test iOS 2','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1558355719.jpg###https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/1558355728.jpg','1558355729'),
(31,'3','2019-05-21','3','1','h4br4h','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190521201414_0.jpg###https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190521201416_1.jpg','1558449859'),
(33,'3','2019-05-22','3','4','mukti test','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190522114631_0.jpg###https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190522114636_1.jpg###https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190522114638_2.jpg','1558505797'),
(34,'3','2019-05-23','3','3','test field report','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190523112053_0.jpg###https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190523112113_1.jpg','1558590683'),
(35,'3','2019-05-23','3','3','test','https://s3-us-west-2.amazonaws.com/dolphino/uploads/2017/09/20190523114532_0.jpg','1558592149');

/*Table structure for table `holidays` */

DROP TABLE IF EXISTS `holidays`;

CREATE TABLE `holidays` (
  `holiday_id` int(11) NOT NULL AUTO_INCREMENT,
  `holiday_name` varchar(256) NOT NULL,
  `holiday_date_from` date NOT NULL,
  `holiday_date_to` date NOT NULL,
  PRIMARY KEY (`holiday_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `holidays` */

insert  into `holidays`(`holiday_id`,`holiday_name`,`holiday_date_from`,`holiday_date_to`) values 
(3,'New Year--','2018-07-02','2018-07-04'),
(5,'valentine day','2019-02-14','2019-12-09'),
(6,'Chinese New Year','2019-02-05','2019-12-01'),
(9,'I think you have to change password! -SSH','2019-12-15','2019-12-16');

/*Table structure for table `machine` */

DROP TABLE IF EXISTS `machine`;

CREATE TABLE `machine` (
  `machine_id` int(11) NOT NULL AUTO_INCREMENT,
  `machine_name` varchar(256) NOT NULL,
  `machine_image` text NOT NULL,
  PRIMARY KEY (`machine_id`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=latin1;

/*Data for the table `machine` */

insert  into `machine`(`machine_id`,`machine_name`,`machine_image`) values 
(148,'Komatsu PC50','JCB.png'),
(149,'Case CX135','JCB.png'),
(150,'Link Belt LX145','JCB.png'),
(151,'John Deere 200D','JCB.png'),
(152,'Link Belt LX225','JCB.png'),
(153,'Case CX245','JCB.png'),
(154,'John Deer 350G','JCB.png'),
(155,'Case TV380','JCB.png'),
(156,'Bomag Roller 56','JCB.png'),
(157,'John Deere 550J','JCB.png'),
(158,'Peterbuilt 220','JCB.png'),
(159,'LX195','JCB.png');

/*Table structure for table `material` */

DROP TABLE IF EXISTS `material`;

CREATE TABLE `material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `materials_name` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `time_set` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

/*Data for the table `material` */

insert  into `material`(`id`,`materials_name`,`amount`,`unit`,`time_set`) values 
(53,'19mm Road Base','39','Tonne','1557261719');

/*Table structure for table `notification` */

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `empl_id` int(11) DEFAULT NULL,
  `text` varchar(4000) DEFAULT NULL,
  `isread` int(11) DEFAULT 0,
  `created_date` datetime DEFAULT current_timestamp(),
  `type` varchar(255) DEFAULT NULL,
  `project_id` int(11) DEFAULT -1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;

/*Data for the table `notification` */

insert  into `notification`(`id`,`empl_id`,`text`,`isread`,`created_date`,`type`,`project_id`) values 
(1,16,'Project Update successfully',1,'2019-05-07 13:29:54','Project_Update',-1),
(2,16,'Project Update successfully',1,'2019-05-07 13:29:54','Project_Update',-1),
(3,16,'Report has been dispatch for date ',1,'2019-05-07 13:32:59','Dispatch_Report',-99),
(4,7,'Project Update successfully',1,'2019-05-07 20:50:19','Project_Update',-1),
(5,8,'Project Update successfully',0,'2019-05-07 20:50:19','Project_Update',-1),
(6,9,'Project Update successfully',0,'2019-05-07 20:50:19','Project_Update',-1),
(7,10,'Project Update successfully',0,'2019-05-07 20:50:19','Project_Update',-1),
(8,11,'Project Update successfully',0,'2019-05-07 20:50:19','Project_Update',-1),
(9,12,'Project Update successfully',0,'2019-05-07 20:50:20','Project_Update',-1),
(10,12,'Project Update successfully',0,'2019-05-07 20:50:20','Project_Update',-1),
(11,3,'Project Update successfully',0,'2019-05-07 20:51:50','Project_Update',-1),
(12,5,'Project Update successfully',1,'2019-05-07 20:51:50','Project_Update',-1),
(13,7,'Project Update successfully',0,'2019-05-07 20:51:50','Project_Update',-1),
(14,8,'Project Update successfully',0,'2019-05-07 20:51:51','Project_Update',-1),
(15,9,'Project Update successfully',0,'2019-05-07 20:51:51','Project_Update',-1),
(16,10,'Project Update successfully',0,'2019-05-07 20:51:51','Project_Update',-1),
(17,11,'Project Update successfully',0,'2019-05-07 20:51:51','Project_Update',-1),
(18,12,'Project Update successfully',0,'2019-05-07 20:51:51','Project_Update',-1),
(19,12,'Project Update successfully',0,'2019-05-07 20:51:52','Project_Update',-1),
(20,3,'Project Update successfully',0,'2019-05-07 20:55:48','Project_Update',-1),
(21,5,'Project Update successfully',1,'2019-05-07 20:55:48','Project_Update',-1),
(22,7,'Project Update successfully',0,'2019-05-07 20:55:49','Project_Update',-1),
(23,8,'Project Update successfully',0,'2019-05-07 20:55:49','Project_Update',-1),
(24,9,'Project Update successfully',0,'2019-05-07 20:55:49','Project_Update',-1),
(25,10,'Project Update successfully',0,'2019-05-07 20:55:49','Project_Update',-1),
(26,11,'Project Update successfully',0,'2019-05-07 20:55:49','Project_Update',-1),
(27,12,'Project Update successfully',0,'2019-05-07 20:55:49','Project_Update',-1),
(28,12,'Project Update successfully',0,'2019-05-07 20:55:50','Project_Update',-1),
(29,3,'Project Update successfully',0,'2019-05-07 20:57:07','Project_Update',-1),
(30,5,'Project Update successfully',1,'2019-05-07 20:57:07','Project_Update',-1),
(31,7,'Project Update successfully',0,'2019-05-07 20:57:07','Project_Update',-1),
(32,8,'Project Update successfully',0,'2019-05-07 20:57:07','Project_Update',-1),
(33,9,'Project Update successfully',0,'2019-05-07 20:57:07','Project_Update',-1),
(34,10,'Project Update successfully',0,'2019-05-07 20:57:07','Project_Update',-1),
(35,11,'Project Update successfully',0,'2019-05-07 20:57:08','Project_Update',-1),
(36,12,'Project Update successfully',0,'2019-05-07 20:57:08','Project_Update',-1),
(37,12,'Project Update successfully',0,'2019-05-07 20:57:08','Project_Update',-1),
(38,3,'Project Update successfully',1,'2019-05-07 20:57:51','Project_Update',-1),
(39,5,'Project Update successfully',1,'2019-05-07 20:57:51','Project_Update',-1),
(40,7,'Project Update successfully',0,'2019-05-07 20:57:51','Project_Update',-1),
(41,8,'Project Update successfully',0,'2019-05-07 20:57:52','Project_Update',-1),
(42,9,'Project Update successfully',0,'2019-05-07 20:57:52','Project_Update',-1),
(43,10,'Project Update successfully',0,'2019-05-07 20:57:52','Project_Update',-1),
(44,11,'Project Update successfully',0,'2019-05-07 20:57:52','Project_Update',-1),
(45,12,'Project Update successfully',0,'2019-05-07 20:57:52','Project_Update',-1),
(46,12,'Project Update successfully',0,'2019-05-07 20:57:53','Project_Update',-1),
(47,3,'Project Update successfully',0,'2019-05-07 20:59:46','Project_Update',-1),
(48,5,'Project Update successfully',1,'2019-05-07 20:59:46','Project_Update',-1),
(49,7,'Project Update successfully',0,'2019-05-07 20:59:47','Project_Update',-1),
(50,8,'Project Update successfully',0,'2019-05-07 20:59:47','Project_Update',-1),
(51,9,'Project Update successfully',0,'2019-05-07 20:59:47','Project_Update',-1),
(52,10,'Project Update successfully',0,'2019-05-07 20:59:47','Project_Update',-1),
(53,11,'Project Update successfully',0,'2019-05-07 20:59:47','Project_Update',-1),
(54,12,'Project Update successfully',0,'2019-05-07 20:59:47','Project_Update',-1),
(55,12,'Project Update successfully',0,'2019-05-07 20:59:48','Project_Update',-1),
(56,3,'Project Update successfully',0,'2019-05-10 08:09:53','Project_Update',-1),
(57,5,'Project Update successfully',1,'2019-05-10 08:09:53','Project_Update',-1),
(58,5,'Project Update successfully',1,'2019-05-10 08:09:53','Project_Update',-1),
(59,3,'Project Update successfully',1,'2019-05-10 08:10:32','Project_Update',-1),
(60,5,'Project Update successfully',1,'2019-05-10 08:10:32','Project_Update',-1),
(61,5,'Project Update successfully',1,'2019-05-10 08:10:32','Project_Update',-1),
(62,3,'Project Update successfully',1,'2019-05-11 07:08:11','Project_Update',-1),
(63,5,'Project Update successfully',1,'2019-05-11 07:08:11','Project_Update',-1),
(64,7,'Project Update successfully',0,'2019-05-11 07:08:11','Project_Update',-1),
(65,8,'Project Update successfully',0,'2019-05-11 07:08:11','Project_Update',-1),
(66,9,'Project Update successfully',0,'2019-05-11 07:08:11','Project_Update',-1),
(67,10,'Project Update successfully',0,'2019-05-11 07:08:11','Project_Update',-1),
(68,11,'Project Update successfully',0,'2019-05-11 07:08:11','Project_Update',-1),
(69,12,'Project Update successfully',0,'2019-05-11 07:08:11','Project_Update',-1),
(70,35,'Project Update successfully',0,'2019-05-11 07:08:11','Project_Update',-1),
(71,35,'Project Update successfully',0,'2019-05-11 07:08:11','Project_Update',-1),
(72,3,'Project Update successfully',1,'2019-05-11 07:08:24','Project_Update',-1),
(73,5,'Project Update successfully',1,'2019-05-11 07:08:24','Project_Update',-1),
(74,35,'Project Update successfully',0,'2019-05-11 07:08:24','Project_Update',-1),
(75,35,'Project Update successfully',0,'2019-05-11 07:08:24','Project_Update',-1),
(76,3,'Project Update successfully',1,'2019-05-11 09:13:37','Project_Update',-1),
(77,5,'Project Update successfully',1,'2019-05-11 09:13:37','Project_Update',-1),
(78,7,'Project Update successfully',0,'2019-05-11 09:13:37','Project_Update',-1),
(79,8,'Project Update successfully',0,'2019-05-11 09:13:37','Project_Update',-1),
(80,9,'Project Update successfully',0,'2019-05-11 09:13:37','Project_Update',-1),
(81,10,'Project Update successfully',0,'2019-05-11 09:13:37','Project_Update',-1),
(82,11,'Project Update successfully',0,'2019-05-11 09:13:37','Project_Update',-1),
(83,12,'Project Update successfully',0,'2019-05-11 09:13:37','Project_Update',-1),
(84,35,'Project Update successfully',0,'2019-05-11 09:13:37','Project_Update',-1),
(85,35,'Project Update successfully',0,'2019-05-11 09:13:37','Project_Update',-1),
(86,3,'Project Update successfully',1,'2019-05-18 09:21:02','Project_Update',-1),
(87,5,'Project Update successfully',1,'2019-05-18 09:21:02','Project_Update',-1),
(88,5,'Project Update successfully',1,'2019-05-18 09:21:02','Project_Update',-1),
(89,5,'Report has been dispatch for date ',0,'2019-05-21 15:44:16','Dispatch_Report',-99),
(90,3,'Project Update successfully',0,'2019-11-21 12:44:52','Project_Update',-1),
(91,5,'Project Update successfully',0,'2019-11-21 12:44:52','Project_Update',-1),
(92,7,'Project Update successfully',0,'2019-11-21 12:44:52','Project_Update',-1),
(93,8,'Project Update successfully',0,'2019-11-21 12:44:52','Project_Update',-1),
(94,9,'Project Update successfully',0,'2019-11-21 12:44:52','Project_Update',-1),
(95,10,'Project Update successfully',0,'2019-11-21 12:44:52','Project_Update',-1),
(96,11,'Project Update successfully',0,'2019-11-21 12:44:52','Project_Update',-1),
(97,12,'Project Update successfully',0,'2019-11-21 12:44:52','Project_Update',-1),
(98,12,'Project Update successfully',0,'2019-11-21 12:44:52','Project_Update',-1),
(99,3,'Project Update successfully',0,'2019-11-21 12:48:07','Project_Update',-1),
(100,5,'Project Update successfully',0,'2019-11-21 12:48:07','Project_Update',-1),
(101,7,'Project Update successfully',0,'2019-11-21 12:48:07','Project_Update',-1),
(102,8,'Project Update successfully',0,'2019-11-21 12:48:07','Project_Update',-1),
(103,9,'Project Update successfully',0,'2019-11-21 12:48:07','Project_Update',-1),
(104,10,'Project Update successfully',0,'2019-11-21 12:48:07','Project_Update',-1),
(105,11,'Project Update successfully',0,'2019-11-21 12:48:07','Project_Update',-1),
(106,12,'Project Update successfully',0,'2019-11-21 12:48:07','Project_Update',-1),
(107,12,'Project Update successfully',0,'2019-11-21 12:48:07','Project_Update',-1),
(108,3,'Project Update successfully',0,'2019-11-21 12:48:17','Project_Update',-1),
(109,5,'Project Update successfully',0,'2019-11-21 12:48:17','Project_Update',-1),
(110,7,'Project Update successfully',0,'2019-11-21 12:48:17','Project_Update',-1),
(111,8,'Project Update successfully',0,'2019-11-21 12:48:17','Project_Update',-1),
(112,9,'Project Update successfully',0,'2019-11-21 12:48:17','Project_Update',-1),
(113,10,'Project Update successfully',0,'2019-11-21 12:48:17','Project_Update',-1),
(114,11,'Project Update successfully',0,'2019-11-21 12:48:17','Project_Update',-1),
(115,12,'Project Update successfully',0,'2019-11-21 12:48:17','Project_Update',-1),
(116,12,'Project Update successfully',0,'2019-11-21 12:48:17','Project_Update',-1),
(117,1,'Report has been dispatch for date ',1,'2019-11-21 15:23:43','Dispatch_Report',-99),
(118,5,'Report has been dispatch for date ',0,'2019-11-21 15:34:07','Dispatch_Report',-99),
(119,5,'Report has been dispatch for date ',0,'2019-11-21 15:36:04','Dispatch_Report',-99),
(120,5,'Report has been dispatch for date ',0,'2019-12-17 18:40:03','Dispatch_Report',-99),
(121,3,'Report has been dispatch for date ',0,'2019-12-17 18:41:26','Dispatch_Report',-99),
(122,3,'Project Update successfully',0,'2019-12-17 18:50:48','Project_Update',-1),
(123,5,'Project Update successfully',0,'2019-12-17 18:50:48','Project_Update',-1),
(124,7,'Project Update successfully',0,'2019-12-17 18:50:48','Project_Update',-1),
(125,8,'Project Update successfully',0,'2019-12-17 18:50:48','Project_Update',-1),
(126,9,'Project Update successfully',0,'2019-12-17 18:50:48','Project_Update',-1),
(127,10,'Project Update successfully',0,'2019-12-17 18:50:48','Project_Update',-1),
(128,11,'Project Update successfully',0,'2019-12-17 18:50:48','Project_Update',-1),
(129,12,'Project Update successfully',0,'2019-12-17 18:50:48','Project_Update',-1),
(130,12,'Project Update successfully',0,'2019-12-17 18:50:48','Project_Update',-1);

/*Table structure for table `project` */

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `Project_id` int(11) NOT NULL AUTO_INCREMENT,
  `Project_name` varchar(256) NOT NULL,
  `Client_id` int(11) NOT NULL,
  `Start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `Rate` int(11) NOT NULL,
  `billing_type` varchar(255) NOT NULL,
  `Total_hours` varchar(100) NOT NULL,
  `Priority` enum('0','1','2') NOT NULL DEFAULT '1',
  `Project_leader` varchar(256) NOT NULL,
  `Team_member` varchar(256) NOT NULL,
  `Project_Address` text NOT NULL,
  `machine` varchar(256) NOT NULL,
  `material` varchar(256) NOT NULL,
  `consumption` varchar(255) NOT NULL,
  `decription` text NOT NULL,
  `images` text NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `manual_project_id` varchar(100) DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  PRIMARY KEY (`Project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `project` */

insert  into `project`(`Project_id`,`Project_name`,`Client_id`,`Start_date`,`end_date`,`Rate`,`billing_type`,`Total_hours`,`Priority`,`Project_leader`,`Team_member`,`Project_Address`,`machine`,`material`,`consumption`,`decription`,`images`,`status`,`manual_project_id`,`total_amount`) values 
(2,'100-19 The Elliot',2,'2019-05-06','2019-06-06',30,'Hourly','151','0','4,19','3,5,7,8,9,10,11,12','45562 Airport road','150,151,153,157','','','<p>Bulk ExcavationÂ </p>','','1','100-18',NULL),
(3,'100-20 The Elliots',2,'2019-05-09','2019-08-02',0,'Hourly','10','0','4','3,5','45562 Airport road','149,151,153,155','','','','','0','100-20',NULL);

/*Table structure for table `project_tasks` */

DROP TABLE IF EXISTS `project_tasks`;

CREATE TABLE `project_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(255) NOT NULL,
  `task_discription` varchar(255) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` varchar(50) DEFAULT 'New',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `project_tasks` */

insert  into `project_tasks`(`id`,`task_name`,`task_discription`,`project_id`,`created_time`,`status`) values 
(2,'Title test','Test task',2,'2019-05-21 15:41:27','New');

/*Table structure for table `task_employee` */

DROP TABLE IF EXISTS `task_employee`;

CREATE TABLE `task_employee` (
  `empl_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `task_employee` */

/*Table structure for table `time_card` */

DROP TABLE IF EXISTS `time_card`;

CREATE TABLE `time_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(100) NOT NULL,
  `employee_id` varchar(500) NOT NULL,
  `deadline` varchar(100) NOT NULL,
  `total_hours` varchar(100) NOT NULL,
  `remain_hours` varchar(100) NOT NULL,
  `work_type` varchar(100) NOT NULL,
  `card_date` varchar(100) NOT NULL,
  `hours` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `machine` varchar(100) NOT NULL,
  `machine_hours` varchar(100) NOT NULL,
  `created_date` date NOT NULL,
  `time_set` varchar(255) NOT NULL,
  `taskid` int(11) DEFAULT NULL,
  `readstatus` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

/*Data for the table `time_card` */

insert  into `time_card`(`id`,`project_name`,`employee_id`,`deadline`,`total_hours`,`remain_hours`,`work_type`,`card_date`,`hours`,`description`,`status`,`machine`,`machine_hours`,`created_date`,`time_set`,`taskid`,`readstatus`) values 
(0,'3','3','2019-05-20','','0','Machines','2019-05-20','5','000','0','155,153','8 ,8 ','2019-05-20','',1,0),
(28,'3','3','2019-05-21','','0','Machines','2019-05-21','5','999','0','150,154','4 ,1 ','2019-05-21','',1,0),
(29,'2','5','2019-05-17','','5','Labour','2019-05-18','5','888','0','148','5','2019-05-21','',1,0),
(30,'2','3','2019-05-23','','0','Machines','2019-05-21','5','777','0','149','3 ','2019-05-21','',1,0),
(34,'3','3','2019-05-23','','0','Machines','2019-05-23','6','666','1','152,157,153,150','8 ,1 ,3 ,2 ','2019-05-23','',2,1),
(35,'2','5','2020-01-05','','0','Machines','2020-01-08','5','555','1','153,157','8','2020-01-08','',2,1),
(36,'2','5','01/07/2020','','138','Machines','2020-01-06','4','444','0','','','2020-01-08','1578496658',2,0),
(37,'2','3','01/07/2020','','138','Machines','01/07/2020','2','333','0','149,153','4','2020-01-08','',2,0),
(38,'2','5','01/08/2020','','138','Machines','01/08/2020','5','222','0','','','2020-01-08','1578497082',2,0),
(39,'2','4','01/10/2020','','93','Machines','01/15/2020','3','1111','0','','','2020-01-11','1578760060',2,0);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(300) NOT NULL,
  `first_name` varchar(300) NOT NULL,
  `last_name` varchar(300) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(300) NOT NULL,
  `birthday` varchar(300) NOT NULL,
  `address` varchar(350) NOT NULL,
  `country` varchar(300) NOT NULL,
  `state` varchar(300) NOT NULL,
  `pin_code` varchar(300) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `user_employee_id` varchar(255) DEFAULT NULL,
  `user_company_id` int(11) DEFAULT NULL,
  `pass_token` varchar(300) NOT NULL,
  `img` varchar(300) NOT NULL,
  `user_role` enum('0','1','2') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `device_id` varchar(255) DEFAULT NULL,
  `device_type` varchar(255) DEFAULT NULL,
  `otp_manage` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`user_id`,`user_name`,`first_name`,`last_name`,`phone`,`email`,`password`,`birthday`,`address`,`country`,`state`,`pin_code`,`gender`,`user_employee_id`,`user_company_id`,`pass_token`,`img`,`user_role`,`created_at`,`updated_at`,`device_id`,`device_type`,`otp_manage`) values 
(31,'travisredekop','Travis','Redekop',0,'travis@silverstoneexcavating.com','e10adc3949ba59abbe56e057f20f883e','','','Canada','','','Male',NULL,1,'','','0','2019-05-07 16:01:06','2019-12-03 11:28:01',NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
