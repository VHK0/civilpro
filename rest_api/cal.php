<?php
// echo $last_date;
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "attodayi_civilpro";

// echo $last_date;
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT * FROM holidays";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    $dataA = [];
    while($row = $result->fetch_assoc()) {
        $second = [
            'title' =>$row["holiday_name"],
            'start' => $row["holiday_date_from"],'end' =>$row["holiday_date_to"],'className' => 'bg-purple'
        ];
        $newA = array_push($dataA, $second);
    }
    header('Content-Type: application/json');
    echo json_encode($dataA);
} else {
    echo "0 results";
}
$conn->close();


?>