<?php
session_start();
require "../config/config.php";
$obj = new connection();
$con = $obj->connect(); 
// echo $last_date;
// $servername = "localhost";
// $username = "root";
// $password = "";
// $dbname = "attodayi_civilpro";
$start_date = $_REQUEST['start_date'];
$end_date = $_REQUEST['end_date'];
$type = $_REQUEST['exportcheck'];
// Create connection
$first_day = substr($start_date,3,2);
$first_month = substr($start_date,0,2);
$first_year = substr($start_date,-4);
$first_date = $first_year."-".$first_month."-".$first_day;
$last_day = substr($end_date,3,2);
$last_month = substr($end_date,0,2);
$last_year = substr($end_date,-4);
$last_date = $last_year."-".$last_month."-".$last_day;
// echo $last_date;
include("mpdf.php");
if($type == '2'){
                                                      $mpdf = new mPDF(); 

                                                      //$html = '<h1>Welcome</h1>';
                                                      $html = '
                                                      <h2>Timesheet Details by Project</h2></h5>(Between '.$first_date.' and '.$last_date.')</h5>
                                                          <table>
                                                              <thead>
                                                                  <tr>
                                                                      <th>Project Name</th>
                                                                      <th>Card Date</th> 
                                                                      <th>Team</th>
                                                                      <th>Client</th>
                                                                      <th>Task</th>
                                                                      <th>Description</th>                      
                                                                      <th>Hours</th>
                                                                      <th>Billed</th>
                                                                      <th>Deadline</th>
                                                                      <th>Machines</th>
                                                                  </tr>
                                                              </thead>
                                                              <tbody>'; ?>
                                                      <?php
                                                              $sql = "SELECT u.card_date,u.deadline, e.first_name AS emp_first_name, e.last_name AS emp_last_name, p.project_name, pt.task_name AS work_type, 
                                                              cl.first_name AS cli_first_name , cl.last_name AS cli_last_name,
                                                              u.description, u.hours, 'yes' AS billed, u.employee_id ,p.Project_id AS project_id
                                                              FROM time_card u
                                                              INNER JOIN employee e ON e.empl_id = u.employee_id
                                                              INNER JOIN Project p ON p.Project_id = u.project_name
                                                              INNER JOIN `Client` cl ON cl.id = p.Client_id
                                                              INNER JOIN project_tasks pt ON pt.id = u.taskid
                                                              WHERE ( STR_TO_DATE(u.card_date,'%m/%d/%Y') >= STR_TO_DATE('$start_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') >= STR_TO_DATE('$start_date','%m/%d/%Y')) AND (STR_TO_DATE(u.card_date,'%m/%d/%Y') <= STR_TO_DATE('$end_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') <= STR_TO_DATE('$end_date','%m/%d/%Y'))";
                                                              $result = mysqli_query($con,$sql);
                                                              
                                                                  if(mysqli_num_rows($result) > 0){
                                                                      $data = [];
                                                                            while($row=mysqli_fetch_assoc($result)){
                                                                                              $second = [
                                                                                                  'project_name' => $row["project_name"],'billed' =>$row["billed"],
                                                                                                  'card_date' => $row["card_date"],'cli_first_name' =>$row["cli_first_name"],
                                                                                                  'cli_last_name' => $row["cli_last_name"],'description' =>$row["description"],
                                                                                                  'emp_first_name' => $row["emp_first_name"],'emp_last_name' =>$row["emp_last_name"],
                                                                                                  'employee_id' => $row["employee_id"],'hours' =>$row["hours"],
                                                                                                  'project_id' => $row["project_id"],'work_type' =>$row["work_type"],'deadline' =>$row["deadline"]
                                                                                              ];
                                                                                              $newA = array_push($data, $second);
                                                                            }
                                                                      $Time_Card = "SELECT u.machine FROM time_card u
                                                                      INNER JOIN employee e ON e.empl_id = u.employee_id
                                                                      INNER JOIN Project p ON p.Project_id = u.project_name
                                                                      INNER JOIN `Client` cl ON cl.id = p.Client_id
                                                                      INNER JOIN project_tasks pt ON pt.id = u.taskid
                                                                      WHERE ( STR_TO_DATE(u.card_date,'%m/%d/%Y') >= STR_TO_DATE('$start_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') >= STR_TO_DATE('$start_date','%m/%d/%Y')) AND (STR_TO_DATE(u.card_date,'%m/%d/%Y') <= STR_TO_DATE('$end_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') <= STR_TO_DATE('$end_date','%m/%d/%Y'))";
                                                                      $Time_Cardd = mysqli_query($con,$Time_Card);
                                                                      $datam = array();
                                                                          if ($Time_Cardd->num_rows > 0) { $j = 0;
                                                                                  $Time_Card_hours = "SELECT u.machine_hours FROM time_card u
                                                                                  INNER JOIN employee e ON e.empl_id = u.employee_id
                                                                                  INNER JOIN Project p ON p.Project_id = u.project_name
                                                                                  INNER JOIN `Client` cl ON cl.id = p.Client_id
                                                                                  INNER JOIN project_tasks pt ON pt.id = u.taskid
                                                                                  WHERE ( STR_TO_DATE(u.card_date,'%m/%d/%Y') >= STR_TO_DATE('$start_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') >= STR_TO_DATE('$start_date','%m/%d/%Y')) AND (STR_TO_DATE(u.card_date,'%m/%d/%Y') <= STR_TO_DATE('$end_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') <= STR_TO_DATE('$end_date','%m/%d/%Y'))";
                                                                                  $Time_Cardd_hours = mysqli_query($con,$Time_Card_hours);
                                                                                  $datah = array();
                                                                                      if ($Time_Cardd_hours->num_rows > 0) { $j = 0;
                                                                                          while($card_data_hours = $Time_Cardd_hours->fetch_assoc()) {
                                                                                              $array_value_hours = $card_data_hours['machine_hours'];
                                                                                              $array_hours =  explode(",", $array_value_hours);
                                                                                              array_push($datah, $array_hours);  
                                                                                            }	   
                                                                                      }
                                                                                        while($card_data = $Time_Cardd->fetch_assoc()) {
                                                                                            $array_value = $card_data['machine'];
                                                                                            $array =  explode(",", $array_value);
                                                                                            $string_version = implode(',', $array);
                                                                                            $get_machine = "select * from machine WHERE `machine_id` IN ($string_version) ";
                                                                                            $get_machinee = mysqli_query($con,$get_machine); $i = 0;
                                                                                            while(@$row=mysqli_fetch_assoc($get_machinee))
                                                                                            {
                                                                                            $datam[$j][$i] = $row['machine_name'];
                                                                                            $i++;
                                                                                            }
                                                                                            if($datam[$j][$i] =""){
                                                                                                $datam[$j][$i] = NULL;
                                                                                            }
                                                                                            $j++;
                                                                                        }	   
                                                                          $string_versions = json_encode($datam);
                                                                          }
                                                                      $z = 0;
                                                                      $temp_id = 0;
                                                                      $totals = 0;
                                                                      $project_temid;
                                                                      $pro_id = $data[0]['project_id'];
                                                                              for($j = 0; $j< count($data); $j++){
                                                                                    $mmm;
                                                                                    $card_date_js;
                                                                                    $emp_first_name_js;
                                                                                    $emp_last_name_js;
                                                                                    $cli_first_name_js;
                                                                                    $cli_last_name_js;
                                                                                    $work_type_js;
                                                                                    $description_js;
                                                                                    $hours_js;
                                                                                    $sub;
                                                                                    if(count($datam[$j]) == 1)
                                                                                    {
                                                                                        $sub = count($datam[$j])-1;

                                                                                    }else{
                                                                                        $sub = count($datam[$j])-2;
                                                                                    }

                                                                                    for ($mmm = 0; $mmm <= $sub; $mmm++) {
                                                                                      $temp;
                                                                                      if($datam[$j][$mmm]){
                                                                                          if($datah[$j][$mmm]){
                                                                                          $temp = $datam[$j][$mmm].'  :  '.$datah[$j][$mmm].' Hrs ';
                                                                                          }else{
                                                                                              $temp = $datam[$j][$mmm];
                                                                                          }

                                                                                      }else{
                                                                                          $temp = "";
                                                                                      }
                                                                                      if( $mmm == 0){

                                                                                          if($temp_id == 0){
                                                                                              $temp_name = $data[$j]['project_name'];
                                                                                              $project_temid = $data[$j]['project_id'];
                                                                                              
                                                                                          }else{
                                                                                              if($temp_id == $data[$j]['project_id']){
                                                                                                $temp_name = "";
                                                                                                  
                                                                                              }else{
                                                                                                  $totals = 0;
                                                                                                          $temp_name = $data[$j]['project_name'];
                                                                                              }
                                                                                          }
                                                                                          if( $project_temid = $data[$j]['project_id'] ){
                                                                                              $totals = $totals + (float)$data[$j]['hours'];

                                                                                          }else{
                                                                                              
                                                                                              $totals = $totals + (float)$data[$j]['hours'];

                                                                                          }
                                                                                          $temp_id = $data[$j]['project_id'];
                                                                                          $project_name = $temp_name;
                                                                                          $machine_inf = $temp;
                                                                                          $emp_last_name_js = $data[$j]['emp_last_name'];
                                                                                          $emp_first_name_js = $data[$j]['emp_first_name'];
                                                                                          $cli_first_name_js = $data[$j]['cli_first_name'];
                                                                                          $cli_last_name_js = $data[$j]['cli_last_name'];
                                                                                          $work_type_js = $data[$j]['work_type'];
                                                                                          $description_js = $data[$j]['description'];
                                                                                          $hours_js = $data[$j]['hours'];
                                                                                          $billing = "Yes";
                                                                                          $card_date_js = $data[$j]['card_date'];
                                                                                          $deadline_js = $data[$j]['deadline'];
                                                                                      }else{
                                                                                          $project_name = "";
                                                                                          $machine_inf = $temp;
                                                                                          $emp_last_name_js = "";
                                                                                          $emp_first_name_js = "";
                                                                                          $cli_first_name_js = "";
                                                                                          $cli_last_name_js = "";
                                                                                          $work_type_js = "";
                                                                                          $description_js = "";
                                                                                          $hours_js = "";
                                                                                          $card_date_js = "";
                                                                                          $billing = "";
                                                                                          $deadline_js = "";
                                                                                      }
                                                                                            $html .= '<tr>
                                                                                            <td>'.$project_name.'</td>
                                                                                            <td>'.$card_date_js.'</td>   
                                                                                            <td>'.$emp_first_name_js.' '.$emp_last_name_js.'</td> 
                                                                                            <td>'.$cli_first_name_js.' '.$cli_last_name_js.'</td> 
                                                                                            <td>'.$work_type_js.'</td>
                                                                                            <td>'.$description_js.'</td>                    
                                                                                            <td>'.$hours_js.'</td>
                                                                                            <td>'.$billing.'</td>
                                                                                            <td>'.$deadline_js.'</td>
                                                                                            <td>'.$machine_inf.'</td>
                                                                                            <td></td>
                                                                                            </tr>';
                                                                                  }
                                                                                  $val = $j + 1;
                                                                                            $count = $data[$val]['project_id'];
                                                                                            if($pro_id == $count){
                                                                                              
                                                                                            }else{
                                                                                                $html .= '<tr>
                                                                                                <td>Total</td>
                                                                                                <td>'.$totals.'</td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>

                                                                                                </tr>'; 
                                                                                            }                   
                                                                                    $pro_id = $count;
                                                                                                          
                                                                              }
                                                          }
                                                        $html .= '</tbody></table>';
                                                        $stylesheet = '<style>'.file_get_contents('assets/bootstrap.min.css').'</style>';  // Read the css file
                                                        $mpdf->WriteHTML($stylesheet,1);  //             
                                                        $mpdf->WriteHTML($html,2); 
                                                        $mpdf->Output('Project_Report.pdf', "D");
                          }else{
                            $mpdf = new mPDF(); 

                            //$html = '<h1>Welcome</h1>';
                            $html = '
                            <h2>Timesheet Details by Person</h2></h5>(Between '.$first_date.' and '.$last_date.')</h5>
                            <table>
                                <thead>
                                    <tr>
                                        <th>Empoloyee</th>
                                        <th>Card Date</th>
                                        <th>Project</th>
                                        <th>Client</th>
                                        <th>Task</th> 
                                        <th>Notes</th>
                                        <th>Hours</th>
                                        <th>Billed</th>
                                        <th>Deadline</th>
                                        <th>Machine</th>
                                    </tr>
                                    </thead>
                                    <tbody>'; ?>
                            <?php
                                                              $sql = "SELECT u.card_date,u.deadline, e.first_name AS emp_first_name, e.last_name AS emp_last_name, p.Project_name, pt.task_name AS work_type, 
                                                              cl.first_name AS cli_first_name , cl.last_name AS cli_last_name,
                                                              u.description, u.hours, 'yes' AS billed, u.employee_id ,p.Project_id AS project_id
                                                              FROM time_card u
                                                              INNER JOIN employee e ON e.empl_id = u.employee_id
                                                              INNER JOIN Project p ON p.Project_id = u.project_name
                                                              INNER JOIN `Client` cl ON cl.id = p.Client_id
                                                              INNER JOIN project_tasks pt ON pt.id = u.taskid
                                                              WHERE ( STR_TO_DATE(u.card_date,'%m/%d/%Y') >= STR_TO_DATE('$start_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') >= STR_TO_DATE('$start_date','%m/%d/%Y')) AND (STR_TO_DATE(u.card_date,'%m/%d/%Y') <= STR_TO_DATE('$end_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') <= STR_TO_DATE('$end_date','%m/%d/%Y'))
                                                              ORDER BY e.first_name";
                                                              $result = mysqli_query($con,$sql);
                                                              
                                                                  if(mysqli_num_rows($result) > 0){
                                                                      $data = [];
                                                                            while($row=mysqli_fetch_assoc($result)){
                                                                                              $second = [
                                                                                                  'project_name' => $row["project_name"],'billed' =>$row["billed"],
                                                                                                  'card_date' => $row["card_date"],'cli_first_name' =>$row["cli_first_name"],
                                                                                                  'cli_last_name' => $row["cli_last_name"],'description' =>$row["description"],
                                                                                                  'emp_first_name' => $row["emp_first_name"],'emp_last_name' =>$row["emp_last_name"],
                                                                                                  'employee_id' => $row["employee_id"],'hours' =>$row["hours"],
                                                                                                  'project_id' => $row["project_id"],'work_type' =>$row["work_type"],'deadline' =>$row["deadline"]
                                                                                              ];
                                                                                              $newA = array_push($data, $second);
                                                                            }
                                                                            $Time_Card = "SELECT u.machine FROM time_card u
                                                                            INNER JOIN employee e ON e.empl_id = u.employee_id
                                                                            INNER JOIN Project p ON p.Project_id = u.project_name
                                                                            INNER JOIN `Client` cl ON cl.id = p.Client_id
                                                                            INNER JOIN project_tasks pt ON pt.id = u.taskid
                                                                            WHERE ( STR_TO_DATE(u.card_date,'%m/%d/%Y') >= STR_TO_DATE('$start_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') >= STR_TO_DATE('$start_date','%m/%d/%Y')) AND (STR_TO_DATE(u.card_date,'%m/%d/%Y') <= STR_TO_DATE('$end_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') <= STR_TO_DATE('$end_date','%m/%d/%Y'))
                                                                            ORDER BY e.first_name";
                                                                      $Time_Cardd = mysqli_query($con,$Time_Card);
                                                                      $datam = array();
                                                                          if ($Time_Cardd->num_rows > 0) { $j = 0;
                                                                            $Time_Card_hours = "SELECT u.machine_hours FROM time_card u
                                                                            INNER JOIN employee e ON e.empl_id = u.employee_id
                                                                            INNER JOIN Project p ON p.Project_id = u.project_name
                                                                            INNER JOIN `Client` cl ON cl.id = p.Client_id
                                                                            INNER JOIN project_tasks pt ON pt.id = u.taskid
                                                                            WHERE ( STR_TO_DATE(u.card_date,'%m/%d/%Y') >= STR_TO_DATE('$start_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') >= STR_TO_DATE('$start_date','%m/%d/%Y')) AND (STR_TO_DATE(u.card_date,'%m/%d/%Y') <= STR_TO_DATE('$end_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') <= STR_TO_DATE('$end_date','%m/%d/%Y'))
                                                                            ORDER BY e.first_name";
                                                                                  $Time_Cardd_hours = mysqli_query($con,$Time_Card_hours);
                                                                                  $datah = array();
                                                                                      if ($Time_Cardd_hours->num_rows > 0) { $j = 0;
                                                                                          while($card_data_hours = $Time_Cardd_hours->fetch_assoc()) {
                                                                                              $array_value_hours = $card_data_hours['machine_hours'];
                                                                                              $array_hours =  explode(",", $array_value_hours);
                                                                                              array_push($datah, $array_hours);  
                                                                                            }	   
                                                                                      }
                                                                                        while($card_data = $Time_Cardd->fetch_assoc()) {
                                                                                            $array_value = $card_data['machine'];
                                                                                            $array =  explode(",", $array_value);
                                                                                            $string_version = implode(',', $array);
                                                                                            $get_machine = "select * from machine WHERE `machine_id` IN ($string_version) ";
                                                                                            $get_machinee = mysqli_query($con,$get_machine); $i = 0;
                                                                                            while(@$row=mysqli_fetch_assoc($get_machinee))
                                                                                            {
                                                                                            $datam[$j][$i] = $row['machine_name'];
                                                                                            $i++;
                                                                                            }
                                                                                            if($datam[$j][$i] =""){
                                                                                                $datam[$j][$i] = NULL;
                                                                                            }
                                                                                            $j++;
                                                                                        }	   
                                                                          $string_versions = json_encode($datam);
                                                                          }
                                                                      $z = 0;
                                                                      $temp_id = 0;
                                                                      $totals = 0;
                                                                      $project_temid;
                                                                      $emp_id = $data[0]['employee_id'];
                                                                              for($j = 0; $j< count($data); $j++){
                                                                                    $mmm;
                                                                                    $card_date_js;
                                                                                    $emp_first_name_js;
                                                                                    $emp_last_name_js;
                                                                                    $cli_first_name_js;
                                                                                    $cli_last_name_js;
                                                                                    $work_type_js;
                                                                                    $description_js;
                                                                                    $hours_js;
                                                                                    $sub;
                                                                                    if(count($datam[$j]) == 1)
                                                                                    {
                                                                                        $sub = count($datam[$j])-1;

                                                                                    }else{
                                                                                        $sub = count($datam[$j])-2;
                                                                                    }

                                                                                    for ($mmm = 0; $mmm <= $sub; $mmm++) {
                                                                                      $temp;
                                                                                      if($datam[$j][$mmm]){
                                                                                          if($datah[$j][$mmm]){
                                                                                          $temp = $datam[$j][$mmm].'  :  '.$datah[$j][$mmm].' Hrs ';
                                                                                          }else{
                                                                                              $temp = $datam[$j][$mmm];
                                                                                          }

                                                                                      }else{
                                                                                          $temp = "";
                                                                                      }
                                                                                      if( $mmm == 0){

                                                                                          if($temp_id == 0){
                                                                                              $temp_name = $data[$j]['emp_first_name'].' '.$data[$j]['emp_last_name'];
                                                                                              $project_temid = $data[$j]['employee_id'];
                                                                                              
                                                                                          }else{
                                                                                              if($temp_id == $data[$j]['employee_id']){
                                                                                                $temp_name = "";
                                                                                                  
                                                                                              }else{
                                                                                                  $totals = 0;
                                                                                                  $temp_name = $data[$j]['emp_first_name'].' '.$data[$j]['emp_last_name'];
                                                                                              }
                                                                                          }
                                                                                          if( $project_temid = $data[$j]['employee_id'] ){
                                                                                              $totals = $totals + (float)$data[$j]['hours'];

                                                                                          }else{
                                                                                              
                                                                                              $totals = $totals + (float)$data[$j]['hours'];

                                                                                          }
                                                                                          $temp_id = $data[$j]['employee_id'];
                                                                                          $project_name = $data[$j]['project_name'];
                                                                                          $machine_inf = $temp;
                                                                                          $emp_last_name_js = $temp_name;
                                                                                          // $emp_first_name_js = $data[$j]['emp_first_name'];
                                                                                          $cli_first_name_js = $data[$j]['cli_first_name'];
                                                                                          $cli_last_name_js = $data[$j]['cli_last_name'];
                                                                                          $work_type_js = $data[$j]['work_type'];
                                                                                          $description_js = $data[$j]['description'];
                                                                                          $hours_js = $data[$j]['hours'];
                                                                                          $billing = "Yes";
                                                                                          $card_date_js = $data[$j]['card_date'];
                                                                                          $deadline_js = $data[$j]['deadline'];
                                                                                      }else{
                                                                                          $project_name = "";
                                                                                          $machine_inf = $temp;
                                                                                          $emp_last_name_js = "";
                                                                                          $emp_first_name_js = "";
                                                                                          $cli_first_name_js = "";
                                                                                          $cli_last_name_js = "";
                                                                                          $work_type_js = "";
                                                                                          $description_js = "";
                                                                                          $hours_js = "";
                                                                                          $card_date_js = "";
                                                                                          $billing = "";
                                                                                          $deadline_js = "";
                                                                                      }
                                                                                            $html .= '<tr>
                                                                                            <td>'.$emp_last_name_js.'</td>
                                                                                            <td>'.$card_date_js.'</td>   
                                                                                            <td>'.$project_name.'</td> 
                                                                                            <td>'.$cli_first_name_js.' '.$cli_last_name_js.'</td> 
                                                                                            <td>'.$work_type_js.'</td>
                                                                                            <td>'.$description_js.'</td>                    
                                                                                            <td>'.$hours_js.'</td>
                                                                                            <td>'.$billing.'</td>
                                                                                            <td>'.$deadline_js.'</td>
                                                                                            <td>'.$machine_inf.'</td>
                                                                                            <td></td>
                                                                                            </tr>';
                                                                                  }
                                                                                  $val = $j + 1;
                                                                                            $count = $data[$val]['employee_id'];
                                                                                            if($emp_id == $count){
                                                                                              
                                                                                            }else{
                                                                                                $html .= '<tr>
                                                                                                <td>Total</td>
                                                                                                <td>'.$totals.'</td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>

                                                                                                </tr>'; 
                                                                                            }                   
                                                                                    $emp_id = $count;
                                                                                                          
                                                                              }
                                                          }
                              $html .= '</tbody></table>';
                              $stylesheet = '<style>'.file_get_contents('assets/bootstrap.min.css').'</style>';  // Read the css file
                              $mpdf->WriteHTML($stylesheet,1);  //             
                              $mpdf->WriteHTML($html,2); 
                              $mpdf->Output('Employee_Report.pdf', "D");



                          }


    

?>
