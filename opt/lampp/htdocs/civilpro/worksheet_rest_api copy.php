<?php
session_start();
require "config/config.php";
$obj = new connection();
$con = $obj->connect(); 
// echo $last_date;
// $servername = "localhost";
// $username = "root";
// $password = "";
// $dbname = "attodayi_civilpro";
$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];
$type = $_POST['type'];
// Create connection
$first_day = substr($start_date,3,2);
$first_month = substr($start_date,0,2);
$first_year = substr($start_date,-4);
$first_date = $first_year."-".$first_month."-".$first_day;
$last_day = substr($end_date,3,2);
$last_month = substr($end_date,0,2);
$last_year = substr($end_date,-4);
$last_date = $last_year."-".$last_month."-".$last_day;
// echo $last_date;
// $conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
// echo $start_date.$end_date.$type;
$sql = "SELECT u.card_date, u.deadline, e.first_name AS emp_first_name, e.last_name AS emp_last_name, p.Project_name, pt.task_name AS work_type,
cl.first_name AS cli_first_name , cl.last_name AS cli_last_name,
u.description, u.hours, u.machine,GROUP_CONCAT(m.machine_name) AS machine_name, 'yes' AS billed, u.employee_id ,p.Project_id AS project_id
FROM machine m INNER JOIN time_card u
INNER JOIN employee e ON e.empl_id = u.employee_id
INNER JOIN Project p ON p.Project_id = u.project_name
INNER JOIN `Client` cl ON cl.id = p.Client_id
INNER JOIN project_tasks pt ON pt.id = u.taskid
WHERE FIND_IN_SET(m.machine_id, u.machine) AND ( STR_TO_DATE(u.card_date,'%m/%d/%Y') >= STR_TO_DATE('$start_date','%m/%d/%Y') or STR_TO_DATE(u.card_date,'%Y-%m-%d') >= STR_TO_DATE('$start_date','%m/%d/%Y')) AND (STR_TO_DATE(u.card_date,'%m/%d/%Y') <= STR_TO_DATE('$end_date','%m/%d/%Y') or STR_TO_DATE(u.card_date,'%Y-%m-%d') <= STR_TO_DATE('$end_date','%m/%d/%Y'))";

$result = mysqli_query($con,$sql);

    if(mysqli_num_rows($result) > 0){
        // output data of each row
         $dataA = [];
    // while($row = $result->fetch_assoc()) {
        while($row=mysqli_fetch_assoc($result)){
            $second = [
                'Project_name' => $row["Project_name"],'billed' =>$row["billed"],
                'card_date' => $row["card_date"],'cli_first_name' =>$row["cli_first_name"],
                'cli_last_name' => $row["cli_last_name"],'description' =>$row["description"],
                'emp_first_name' => $row["emp_first_name"],'emp_last_name' =>$row["emp_last_name"],
                'employee_id' => $row["employee_id"],'hours' =>$row["hours"],
                'project_id' => $row["project_id"],'work_type' =>$row["work_type"],
                'machine_name' =>$row["machine_name"],'deadline' =>$row["deadline"]
            ];
            $newA = array_push($dataA, $second);
        }
    header('Content-Type: application/json');
    echo json_encode( $dataA  );
    }else{
        echo"none results";
    }
    
   

mysqli_close($con);


?>