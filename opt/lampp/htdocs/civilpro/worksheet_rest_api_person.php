<?php
session_start();
require "config/config.php";
$obj = new connection();
$con = $obj->connect(); 
$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];
$type = $_POST['type'];
$first_day = substr($start_date,3,2);
$first_month = substr($start_date,0,2);
$first_year = substr($start_date,-4);
$first_date = $first_year."-".$first_month."-".$first_day;
$last_day = substr($end_date,3,2);
$last_month = substr($end_date,0,2);
$last_year = substr($end_date,-4);
$last_date = $last_year."-".$last_month."-".$last_day;
    $sql = "SELECT u.card_date,u.deadline, e.first_name AS emp_first_name, e.last_name AS emp_last_name, p.Project_name, pt.task_name AS work_type, 
    cl.first_name AS cli_first_name , cl.last_name AS cli_last_name,
    u.description, u.hours, 'yes' AS billed, u.employee_id ,p.Project_id AS project_id
    FROM time_card u
    INNER JOIN employee e ON e.empl_id = u.employee_id
    INNER JOIN Project p ON p.Project_id = u.project_name
    INNER JOIN `Client` cl ON cl.id = p.Client_id
    INNER JOIN project_tasks pt ON pt.id = u.taskid
    WHERE ( STR_TO_DATE(u.card_date,'%m/%d/%Y') >= STR_TO_DATE('$start_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') >= STR_TO_DATE('$start_date','%m/%d/%Y')) AND (STR_TO_DATE(u.card_date,'%m/%d/%Y') <= STR_TO_DATE('$end_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') <= STR_TO_DATE('$end_date','%m/%d/%Y'))
    ORDER BY e.first_name";
    $result = mysqli_query($con,$sql);
    
        if(mysqli_num_rows($result) > 0){
             $dataA = [];
            while($row=mysqli_fetch_assoc($result)){
                $second = [
                    'Project_name' => $row["Project_name"],'billed' =>$row["billed"],
                    'card_date' => $row["card_date"],'cli_first_name' =>$row["cli_first_name"],
                    'cli_last_name' => $row["cli_last_name"],'description' =>$row["description"],
                    'emp_first_name' => $row["emp_first_name"],'emp_last_name' =>$row["emp_last_name"],
                    'employee_id' => $row["employee_id"],'hours' =>$row["hours"],
                    'project_id' => $row["project_id"],'work_type' =>$row["work_type"],'deadline' =>$row["deadline"]
                ];
                $newA = array_push($dataA, $second);
            }
            $Time_Card = "SELECT u.machine FROM time_card u
            INNER JOIN employee e ON e.empl_id = u.employee_id
            INNER JOIN Project p ON p.Project_id = u.project_name
            INNER JOIN `Client` cl ON cl.id = p.Client_id
            INNER JOIN project_tasks pt ON pt.id = u.taskid
            WHERE ( STR_TO_DATE(u.card_date,'%m/%d/%Y') >= STR_TO_DATE('$start_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') >= STR_TO_DATE('$start_date','%m/%d/%Y')) AND (STR_TO_DATE(u.card_date,'%m/%d/%Y') <= STR_TO_DATE('$end_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') <= STR_TO_DATE('$end_date','%m/%d/%Y'))
            ORDER BY e.first_name";
            $Time_Cardd = mysqli_query($con,$Time_Card);
            $NewArrayname = array();
                if ($Time_Cardd->num_rows > 0) { $j = 0;
                                $Time_Card_hours = "SELECT u.machine_hours FROM time_card u
                                INNER JOIN employee e ON e.empl_id = u.employee_id
                                INNER JOIN Project p ON p.Project_id = u.project_name
                                INNER JOIN `Client` cl ON cl.id = p.Client_id
                                INNER JOIN project_tasks pt ON pt.id = u.taskid
                                WHERE ( STR_TO_DATE(u.card_date,'%m/%d/%Y') >= STR_TO_DATE('$start_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') >= STR_TO_DATE('$start_date','%m/%d/%Y')) AND (STR_TO_DATE(u.card_date,'%m/%d/%Y') <= STR_TO_DATE('$end_date','%m/%d/%Y') OR STR_TO_DATE(u.card_date,'%Y-%m-%d') <= STR_TO_DATE('$end_date','%m/%d/%Y'))
                                ORDER BY e.first_name";
                                $Time_Cardd_hours = mysqli_query($con,$Time_Card_hours);
                                $NewArrayname_hours = array();
                                    if ($Time_Cardd_hours->num_rows > 0) { $j = 0;
                                        while($card_data_hours = $Time_Cardd_hours->fetch_assoc()) {
                                        
                                            $array_value_hours = $card_data_hours['machine_hours'];
                                            $array_hours =  explode(",", $array_value_hours);
                                            array_push($NewArrayname_hours, $array_hours);  
                                            
                                    }	   
                                    }else{
                                        
                                    }
                    while($card_data = $Time_Cardd->fetch_assoc()) {
                        $array_value = $card_data['machine'];
                        $array =  explode(",", $array_value);
                        $string_version = implode(',', $array);
                        $get_machine = "select * from machine WHERE `machine_id` IN ($string_version) ";
                        $get_machinee = mysqli_query($con,$get_machine); $i = 0;
                        while(@$row=mysqli_fetch_assoc($get_machinee))
                        {
                        $NewArrayname[$j][$i] = $row['machine_name'];
                        $i++;
                        }
                        if($NewArrayname[$j][$i] =""){
                            $NewArrayname[$j][$i] = NULL;
                        }
                        $j++;
                }	   
                 $string_versions = json_encode($NewArrayname);
                }else{
                }
        header('Content-Type: application/json');
        echo json_encode([$dataA, $NewArrayname, $NewArrayname_hours]);
        }else{
            echo"none results";
        }
mysqli_close($con);
?>
