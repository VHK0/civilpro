<?php
session_start();
require "../config/config.php";
$obj = new connection();
$con = $obj->connect(); 
// echo $last_date;
// $servername = "localhost";
// $username = "root";
// $password = "";
// $dbname = "attodayi_civilpro";
$start_date = $_REQUEST['start_date'];
$end_date = $_REQUEST['end_date'];
$type = $_REQUEST['exportcheck'];
// Create connection
$first_day = substr($start_date,3,2);
$first_month = substr($start_date,0,2);
$first_year = substr($start_date,-4);
$first_date = $first_year."-".$first_month."-".$first_day;
$last_day = substr($end_date,3,2);
$last_month = substr($end_date,0,2);
$last_year = substr($end_date,-4);
$last_date = $last_year."-".$last_month."-".$last_day;
// echo $last_date;
include("mpdf.php");
if($type == '2'){
                                                      $mpdf = new mPDF(); 

                                                      //$html = '<h1>Welcome</h1>';
                                                      $html = '
                                                      <h2>Timesheet Details by Project</h2></h5>(Between '.$first_date.' and '.$last_date.')</h5>
                                                          <table>
                                                              <thead>
                                                                  <tr>
                                                                      <th>Team</th>
                                                                      <th>Client</th> 
                                                                      <th>Task</th>
                                                                      <th>Description</th>                      
                                                                      <th>Hours</th>
                                                                      <th>Machines</th>
                                                                      <th>Billed</th>
                                                                      <th>Deadline</th>
                                                                  </tr>
                                                              </thead>
                                                              <tbody>'; ?>
                                                      <?php
                                                        $sql = "SELECT u.card_date, u.deadline, e.first_name AS emp_first_name, e.last_name AS emp_last_name, p.Project_name, pt.task_name AS work_type,
                                                        cl.first_name AS cli_first_name , cl.last_name AS cli_last_name,
                                                        u.description, u.hours, u.machine,GROUP_CONCAT(m.machine_name) AS machine_name, 'yes' AS billed, u.employee_id ,p.Project_id AS project_id
                                                        FROM machine m INNER JOIN time_card u
                                                        INNER JOIN employee e ON e.empl_id = u.employee_id
                                                        INNER JOIN Project p ON p.Project_id = u.project_name
                                                        INNER JOIN `Client` cl ON cl.id = p.Client_id
                                                        INNER JOIN project_tasks pt ON pt.id = u.taskid
                                                        WHERE FIND_IN_SET(m.machine_id, u.machine) AND ( STR_TO_DATE(u.card_date,'%m/%d/%Y') >= STR_TO_DATE('$start_date','%m/%d/%Y') or STR_TO_DATE(u.card_date,'%Y-%m-%d') >= STR_TO_DATE('$start_date','%m/%d/%Y')) AND (STR_TO_DATE(u.card_date,'%m/%d/%Y') <= STR_TO_DATE('$end_date','%m/%d/%Y') or STR_TO_DATE(u.card_date,'%Y-%m-%d') <= STR_TO_DATE('$end_date','%m/%d/%Y'))";
                                                        $result = mysqli_query($con,$sql);
                                                        
                                                          while($row=mysqli_fetch_assoc($result)){
                                                            if(!$row['emp_first_name']){
                                                              $row['billed'] ="";
                                                            }   
                                                            $html .= '<tr>
                                                            <td>'.$row['emp_first_name'].' '.$row['emp_last_name'].'</td>   
                                                            <td>'.$row['cli_first_name'].' '.$row['cli_last_name'].'</td> 
                                                            <td>'.$row['work_type'].'</td>
                                                            <td>'.$row['description'].'</td>                    
                                                            <td>'.$row['hours'].'</td>
                                                            <td>'.$row['machine_name'].'</td>
                                                            <td>'.$row['billed'].'</td>
                                                            <td>'.$row['deadline'].'</td>
                                                            </tr>'; $sum += (int)$row['hours'];
                                                            
                                                          }
                                                        $html .= '</tbody></table><h3>Total: '.$sum.'</h3>';
                                                        $stylesheet = '<style>'.file_get_contents('assets/bootstrap.min.css').'</style>';  // Read the css file
                                                        $mpdf->WriteHTML($stylesheet,1);  //             
                                                        $mpdf->WriteHTML($html,2); 
                                                        $mpdf->Output('Project_Report.pdf', "D");
                          }else{
                            $mpdf = new mPDF(); 

                            //$html = '<h1>Welcome</h1>';
                            $html = '
                            <h2>Timesheet Details by Person</h2></h5>(Between '.$first_date.' and '.$last_date.')</h5>
                            <table>
                                <thead>
                                    <tr>
                                        <th>Team</th>
                                        <th>Client</th> 
                                        <th>Project</th>
                                        <th>Task</th>                      
                                        <th>Notes</th>
                                        <th>Hours</th>
                                        <th>Billed</th>
                                        <th>Deadline</th>
                                    </tr>
                                    </thead>
                                    <tbody>'; ?>
                            <?php
                              $sql = "SELECT u.card_date, u.deadline, e.first_name AS emp_first_name, e.last_name AS emp_last_name, p.Project_name, pt.task_name AS work_type,
                              cl.first_name AS cli_first_name , cl.last_name AS cli_last_name,
                              u.description, u.hours, u.machine,GROUP_CONCAT(m.machine_name) AS machine_name, 'yes' AS billed, u.employee_id ,p.Project_id AS project_id
                              FROM machine m INNER JOIN time_card u
                              INNER JOIN employee e ON e.empl_id = u.employee_id
                              INNER JOIN Project p ON p.Project_id = u.project_name
                              INNER JOIN `Client` cl ON cl.id = p.Client_id
                              INNER JOIN project_tasks pt ON pt.id = u.taskid
                              WHERE FIND_IN_SET(m.machine_id, u.machine) AND ( STR_TO_DATE(u.card_date,'%m/%d/%Y') >= STR_TO_DATE('$start_date','%m/%d/%Y') or STR_TO_DATE(u.card_date,'%Y-%m-%d') >= STR_TO_DATE('$start_date','%m/%d/%Y')) AND (STR_TO_DATE(u.card_date,'%m/%d/%Y') <= STR_TO_DATE('$end_date','%m/%d/%Y') or STR_TO_DATE(u.card_date,'%Y-%m-%d') <= STR_TO_DATE('$end_date','%m/%d/%Y'))";
                              $result = mysqli_query($con,$sql);
                            
                                while($row=mysqli_fetch_assoc($result)){
                                  if(!$row['emp_first_name']){
                                    $row['billed'] ="";
                                  }  
                                  $html .= '<tr>
                                  <td>'.$row['emp_first_name'].' '.$row['emp_last_name'].'</td>   
                                  <td>'.$row['cli_first_name'].' '.$row['cli_last_name'].'</td> 
                                  <td>'.$row['Project_name'].'</td>
                                  <td>'.$row['work_type'].'</td>                    
                                  <td>'.$row['description'].'</td>
                                  <td>'.$row['hours'].'</td>
                                  <td>'.$row['billed'].'</td>
                                  <td>'.$row['deadline'].'</td>
                                  </tr>'; $sum += (int)$row['hours'];
                                  
                                }
                              $html .= '</tbody></table><h3>Total: '.$sum.'</h3>';
                              $stylesheet = '<style>'.file_get_contents('assets/bootstrap.min.css').'</style>';  // Read the css file
                              $mpdf->WriteHTML($stylesheet,1);  //             
                              $mpdf->WriteHTML($html,2); 
                              $mpdf->Output('Employee_Report.pdf', "D");



                          }


    

?>
